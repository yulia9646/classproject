#class student, has name, year, gpa
class Student:
  def __init__(self, name: str, year: int, gpa: float = 0.00)
    self.name = name
    self.year = year
    self.gpa = gpa
    self.grades = []
  def __validate_gpa_is_float(self, gpa): #check if gpa is numeric, if not raise an error
    if not isinstance(gpa, (int, float)):
      raise TypeError(f"{gpa} should be float")
  def __validate_gpa_within_limits(self, gpa):
    if gpa not in range(1,5):
      raise ValueError(f"{gpa} is outside limits")
  def rank_of_student_grade(self):
    self.__validate_gpa_within_limits(self.gpa)
    if self.gpa >= 4.5:
      return "Excellent"
    elif self.gpa >= 3.5 and self.gpa < 4.5:
      return "Good"
    elif self.gpa >= 2.5 and self.gpa < 3.5:
      return "Average"
    else:
      return "Poor"


  def add_grade(self, grade):
    if type(grade) is Grade:
      self.grades.append(grade)
    else:
      pass

  def get_average(self, grades): #get average student score
    return sum(grades) / len(grades)

class Grade:
  minimum_passing = 65
  def __init__(self, score):
    self.score = score
  def is_passing(self, grade): #check if a grade is passing
    if grade > minimum_passing:
      return "You passed!"
    else:
      return "You failed!"
#instances of student class
roger = Student("Roger van der Weyden", 10)
sandro = Student("Sandro Botticelli", 12)
pieter = Student("Pieter Bruegel the Elder", 8)
pieter.add_grade(Grade(100))

